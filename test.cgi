#!/usr/bin/env python
# -*- coding: utf-8 -*-

print "Content-Type: text/plain"
print
print "hello world 5."

import pymysql

#create and pymysql object and assign the credentials
my_con=pymysql.connect(database='621', user='root', password='root', host='127.0.0.1', port=8889)
c=my_con.cursor()

#clear the database if not empty
c.execute("TRUNCATE mytable")

#insert data into database
c.execute("INSERT INTO mytable VALUES(1,'One')")
c.execute("INSERT INTO mytable VALUES(2,'Two')")

#commit anytime you make change
my_con.commit()

#print the contents of database
c.execute("Select * FROM mytable")
print(c.fetchall())


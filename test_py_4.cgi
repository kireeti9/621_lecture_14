#!/usr/bin/env python
import cgitb
from fractions import gcd

import cgi
cgitb.enable()
form=cgi.FieldStorage()

my_num1=int(form.getvalue('my_number_1'))
my_num2=int(form.getvalue('my_number_2'))

print "Content-type: text/html\n\n";
print("<html>")
print("<head>")
print("<title>Exercise 4</title>")
print("</head>")
print("<body>")
print("Greatest Common Divisors of two numbers")
print(my_num1)
print(",")
print(my_num2)
print("is ----")
print(gcd(my_num1, my_num2))
print("</body>")
print("</html>")

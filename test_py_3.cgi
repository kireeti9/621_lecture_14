#!/usr/bin/env python
import cgitb

import cgi
cgitb.enable()
form=cgi.FieldStorage()
my_num=int(form.getvalue('my_number'))
print "Content-type: text/html\n\n";
print()
print("<html>")
print("<head>")
print("<title>Exercise 3</title>")
print("</head>")
print("<body>")
print("Square root of your number is")
print(my_num**.5)
print("</body>")
print("</html>")
